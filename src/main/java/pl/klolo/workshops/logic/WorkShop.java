package pl.klolo.workshops.logic;

import pl.klolo.workshops.domain.*;
import pl.klolo.workshops.domain.Currency;
import pl.klolo.workshops.mock.HoldingMockGenerator;
import pl.klolo.workshops.mock.UserMockGenerator;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

class WorkShop {
    /**
     * Lista holdingów wczytana z mocka.
     */
    private final List<Holding> holdings;

    private final Predicate<User> isWoman = null;

    WorkShop() {
        final HoldingMockGenerator holdingMockGenerator = new HoldingMockGenerator();
        holdings = holdingMockGenerator.generate();
    }

    /**
     * Metoda zwraca liczbę holdingów w których jest przynajmniej jedna firma.
     */
    long getHoldingsWhereAreCompanies() {
        return holdings.stream()
                .filter(c -> c.getCompanies().size() > 0)
                .count();
    }

    /**
     * Zwraca nazwy wszystkich holdingów pisane z małej litery w formie listy.
     */
    List<String> getHoldingNames() {
        return holdings.stream()
                .map(x -> x.getName().toLowerCase())
                .collect(toList());
    }

    /**
     * Zwraca nazwy wszystkich holdingów sklejone w jeden string i posortowane.
     * String ma postać: (Coca-Cola, Nestle, Pepsico)
     */
    String getHoldingNamesAsString() {
        return holdings.stream()
                .map(Holding::getName)
                .sorted()
                .collect(Collectors.joining(", ", "(", ")"));
    }

    /**
     * Zwraca liczbę firm we wszystkich holdingach.
     */
    long getCompaniesAmount() {
        return holdings.stream()
                .mapToInt(x -> x.getCompanies().size()).sum();
    }

    /**
     * Zwraca liczbę wszystkich pracowników we wszystkich firmach.
     */
    long getAllUserAmount() {
        return holdings.stream()
                .mapToLong(x -> x.getCompanies()
                        .stream()
                        .mapToInt(y -> y.getUsers().size())
                        .sum()).sum();
    }

    /**
     * Zwraca listę wszystkich nazw firm w formie listy.
     * Tworzenie strumienia firm umieść w osobnej metodzie którą
     * później będziesz wykorzystywać.
     */
    List<String> getAllCompaniesNames() {
        return getCompanyStream()
                .map(Company::getName).collect(toList());
    }


    /**
     * Zwraca listę wszystkich firm jako listę, której implementacja to LinkedList. Obiektów nie przepisujemy po zakończeniu
     * działania strumienia.
     */
    LinkedList<String> getAllCompaniesNamesAsLinkedList() {
        return getCompanyStream()
                .map(Company::getName)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Zwraca listę firm jako string gdzie poszczególne firmy są oddzielone od siebie znakiem "+"
     */
    String getAllCompaniesNamesAsString() {
        return getCompanyStream()
                .map(Company::getName)
                .collect(Collectors.joining("+"));
    }

    /**
     * Zwraca listę firm jako string gdzie poszczególne firmy są oddzielone od siebie znakiem "+".
     * Używamy collect i StringBuilder.
     * <p>
     * UWAGA: Zadanie z gwiazdką. Nie używamy zmiennych.
     */
    String getAllCompaniesNamesAsStringUsingStringBuilder() {
        return getCompanyStream()
                .map(Company::getName)
                .collect(Collector.of(StringBuilder::new, (x, s) -> {
                            if (x.toString().isEmpty()) {
                                x.append(s);
                            } else {
                                x.append("+").append(s);
                            }
                        },
                        StringBuilder::append,
                        StringBuilder::toString));
    }

    /**
     * Zwraca liczbę wszystkich rachunków, użytkowników we wszystkich firmach.
     */
    long getAllUserAccountsAmount() {
        return getCompanyStream()
                .flatMap(company -> company.getUsers().stream())
                .mapToInt(user -> user.getAccounts().size())
                .sum();
    }

    /**
     * Zwraca listę wszystkich walut w jakich są rachunki jako string, w którym wartości
     * występują bez powtórzeń i są posortowane.
     */
    String getAllCurrencies() {
        final List<String> curriences = getAllCurrenciesAsList();

        return curriences.stream()
                .distinct()
                .sorted()
                .collect(Collectors.joining(", "));
    }

    private List<String> getAllCurrenciesAsList() {
        return getCompanyStream()
                .flatMap(company -> company.getUsers()
                        .stream()
                        .flatMap(user -> user.getAccounts()
                                .stream()
                                .map(account -> account.getCurrency().name())))
                .distinct().collect(Collectors.toList());
    }

    /**
     * Metoda zwraca analogiczne dane jak getAllCurrencies, jednak na utworzonym zbiorze
     * nie uruchamiaj metody
     * stream, tylko skorzystaj z  Stream.generate. Wspólny kod wynieś do osobnej metody.
     *
     * @see #getAllCurrencies()
     */
    String getAllCurrenciesUsingGenerate() {
        final List<String> curriences = getAllCurrenciesAsList();
        return Stream.generate(curriences.iterator()::next)
                .limit(curriences.size())
                .sorted()
                .collect(Collectors.joining(", "));
    }

    /**
     * Zwraca liczbę kobiet we wszystkich firmach. Powtarzający się fragment kodu tworzący strumień uzytkowników umieść w osobnej
     * metodzie. Predicate określający czy mamy doczynienia z kobietą inech będzie polem statycznym w klasie.
     */
    long getWomanAmount() {
        return getUserStream().filter(WorkShop::isWoman).count();
    }

    static private boolean isWoman(User user) {
        return user.getSex().equals(Sex.WOMAN);
    }

    /**
     * Przelicza kwotę na rachunku na złotówki za pomocą kursu określonego w enum Currency.
     */
    BigDecimal getAccountAmountInPLN(final Account account) {
        return account.getAmount()
                .multiply(BigDecimal.valueOf(account.getCurrency().rate))
                .round(new MathContext(4, RoundingMode.HALF_UP));
    }

    /**
     * Przelicza kwotę na podanych rachunkach na złotówki za pomocą kursu określonego w enum Currency  i sumuje ją.
     */
    BigDecimal getTotalCashInPLN(final List<Account> accounts) {
        final Optional<BigDecimal> bigDecimal = accounts.stream()
                .map(x -> x.getAmount()
                        .multiply(BigDecimal.valueOf(x.getCurrency().rate)))
                .reduce(BigDecimal::add);
        return bigDecimal.orElseGet(() -> new BigDecimal(0));
    }

    /**
     * Zwraca imiona użytkowników w formie zbioru, którzy spełniają podany warunek.
     */
    Set<String> getUsersForPredicate(final Predicate<User> userPredicate) {
        return getUserStream()
                .filter(userPredicate)
                .map(User::getFirstName)
                .collect(Collectors.toSet());
    }

    /**
     * Metoda filtruje użytkowników starszych niż podany jako parametr wiek, wyświetla ich na konsoli, odrzuca mężczyzn
     * i zwraca ich imiona w formie listy.
     */
    List<String> getOldWoman(final int age) {
        return getUserStream()
                .filter(user -> user.getAge() > age && user.getSex().equals(Sex.WOMAN))
                .map(User::getFirstName)
                .collect(Collectors.toList());
    }

    /**
     * Dla każdej firmy uruchamia przekazaną metodę.
     */
    void executeForEachCompany(final Consumer<Company> consumer) {
        getCompanyStream().forEach(consumer);
    }

    /**
     * Wyszukuje najbogatsza kobietę i zwraca ja. Metoda musi uzwględniać to że rachunki są w różnych walutach.
     */
    Optional<User> getRichestWoman() {
        final BigDecimal maxValue = getMaxValueAccountOfWoman();
        return getUserStream()
                .filter(user -> user.getSex().equals(Sex.WOMAN) &&
                        getSumOfAccountPLN(user).equals(maxValue)).findFirst();
    }

    private BigDecimal getMaxValueAccountOfWoman() {
        return getUserStream()
                .filter(user -> user.getSex().equals(Sex.WOMAN))
                .map(this::getSumOfAccountPLN)
                .reduce(BigDecimal::max).orElse(new BigDecimal(0));
    }

    private BigDecimal getSumOfAccountPLN(User user) {
        return user.getAccounts()
                .stream()
                .map(x -> x.getAmount()
                        .multiply(BigDecimal.valueOf(Currency.PLN.rate)))
                .reduce(BigDecimal::add).orElse(new BigDecimal(0));
    }

    /**
     * Zwraca nazwy pierwszych N firm. Kolejność nie ma znaczenia.
     */
    Set<String> getFirstNCompany(final int n) {
        return getCompanyStream()
                .map(Company::getName)
                .distinct()
                .limit(n)
                .collect(Collectors.toSet());
    }

    /**
     * Metoda zwraca jaki rodzaj rachunku jest najpopularniejszy. Stwórz pomocniczą metdę getAccountStream.
     * Jeżeli nie udało się znaleźć najpopularnijeszego rachunku metoda ma wyrzucić wyjątek IllegalStateException.
     * Pierwsza instrukcja metody to return.
     */
    AccountType getMostPopularAccountType() {
        return Optional.of(Collections.max(
                getAccoutStream()
                        .map(Account::getType)
                        .collect(groupingBy(Function.identity(), Collectors.counting()))
                        .entrySet(),
                Comparator.comparingLong(Map.Entry::getValue))
                .getKey())
                .orElseThrow(IllegalStateException::new);
    }

    /**
     * Zwraca pierwszego z brzegu użytkownika dla podanego warunku. W przypadku kiedy nie znajdzie użytkownika wyrzuca wyjątek
     * IllegalArgumentException.
     */
    User getUser(final Predicate<User> predicate) {
        return getUserStream()
                .filter(predicate)
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    /**
     * Zwraca mapę firm, gdzie kluczem jest jej nazwa a wartością lista pracowników.
     */
    Map<String, List<User>> getUserPerCompany() {
        return getCompanyStream()
                .collect(
                        Collectors.toMap(Company::getName, Company::getUsers)
                );

    }


    /**
     * Zwraca mapę firm, gdzie kluczem jest jej nazwa a wartością lista pracowników przechowywanych jako string
     * składający się z imienia i nazwiska. Podpowiedź:  Możesz skorzystać z metody entrySet.
     */
    Map<String, List<String>> getUserPerCompanyAsString() {
        return getCompanyStream()
                .collect(
                        Collectors.toMap(
                                Company::getName,
                                company -> company.getUsers()
                                        .stream()
                                        .map(user -> user.getFirstName()
                                                .concat(" ")
                                                .concat(user.getLastName()))
                                        .collect(Collectors.toList())
                        )
                );
    }

    /**
     * Zwraca mapę firm, gdzie kluczem jest jej nazwa a wartością lista pracowników przechowywanych jako obiekty
     * typu T, tworzonych za pomocą przekazanej funkcji.
     */
    <T> Map<String, List<T>> getUserPerCompany(final Function<User, T> converter) {
        return getCompanyStream()
                .collect(
                        Collectors.toMap(
                                Company::getName,
                                c -> c.getUsers()
                                        .stream()
                                        .map(converter)
                                        .collect(Collectors.toList())
                        )
                );
    }

    /**
     * Zwraca mapę gdzie kluczem jest flaga mówiąca o tym czy mamy do czynienia z mężczyzną, czy z kobietą.
     * Osoby "innej" płci mają zostać zignorowane. Wartością jest natomiast zbiór nazwisk tych osób.
     */
    Map<Boolean, Set<String>> getUserBySex() {
        return getUserStream()
                .filter(user -> user.getSex().equals(Sex.MAN) | user.getSex().equals(Sex.WOMAN))
                .collect(partitioningBy(
                        user -> user.getSex().equals(Sex.MAN),
                        mapping(User::getLastName, toSet()))
                );
    }

    /**
     * Zwraca mapę rachunków, gdzie kluczem jesy numer rachunku, a wartością ten rachunek.
     */
    Map<String, Account> createAccountsMap() {
        return getAccoutStream()
                .collect(Collectors.toMap(
                        Account::getNumber,
                        account -> account
                ));
    }

    /**
     * Zwraca listę wszystkich imion w postaci Stringa, posortowanych, gdzie imiona oddzielone są spacją i nie zawierają powtórzeń.
     */
    String getUserNames() {
        return getUserStream()
                .map(User::getFirstName)
                .distinct()
                .sorted()
                .collect(Collectors.joining(" "));
    }

    /**
     * zwraca zbiór wszystkich użytkowników. Jeżeli jest ich więcej niż 10 to obcina ich ilość do 10.
     */
    Set<User> getUsers() {
        return getUserStream().limit(10).collect(toSet());
    }

    /**
     * Zapisuje listę numerów rachunków w pliku na dysku, gdzie w każda linijka wygląda następująco:
     * NUMER_RACHUNKU|KWOTA|WALUTA
     * <p>
     * Skorzystaj z strumieni i try-resources.
     */
    void saveAccountsInFile(final String fileName) {

        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(fos));
            dos.writeBytes(
                    getAccoutStream()
                            .map(this::accountToStringSpecialFormat)
                            .collect(joining("\n"))
            );
            dos.close();

        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        } catch (IOException e) {
            System.out.println("error occured");
        }
    }

    private String accountToStringSpecialFormat(Account account) {
        return account.getNumber() +
                '|' +
                account.getAmount() +
                '|' +
                account.getCurrency();
    }

    /**
     * Zwraca użytkownika, który spełnia podany warunek.
     */
    Optional<User> findUser(final Predicate<User> userPredicate) {
        return getUserStream().filter(userPredicate).findAny();
    }

    /**
     * Dla podanego użytkownika zwraca informacje o tym ile ma lat w formie:
     * IMIE NAZWISKO ma lat X. Jeżeli użytkownik nie istnieje to zwraca text: Brak użytkownika.
     * <p>
     * Uwaga: W prawdziwym kodzie nie przekazuj Optionali jako parametrów.
     */
    String getAdultantStatus(final Optional<User> optionalUser) {
        return optionalUser.map(user -> user.getFirstName() +
                ' ' + user.getLastName() +
                " ma lat " + user.getAge()).orElse("Brak użytkownika");
    }

    /**
     * Metoda wypisuje na ekranie wszystkich użytkowników (imie, nazwisko) posortowanych od z do a.
     * Zosia Psikuta, Zenon Kucowski, Zenek Jawowy ... Alfred Pasibrzuch, Adam Wojcik
     */
    String showAllUser() {
        return getUserStream()
                .map(user -> user.getFirstName()
                        + " " + user.getLastName())
                .sorted(Collections.reverseOrder())
                .collect(Collectors.joining(", "));
    }

    /**
     * Zwraca mapę, gdzie kluczem jest typ rachunku a wartością kwota wszystkich środków
     * na rachunkach tego typu przeliczona na złotówki.
     */
    Map<AccountType, BigDecimal> getMoneyOnAccounts() {
        return getAccoutStream()
                .distinct()
                .collect(Collectors
                        .toMap(Account::getType,
                                account -> account.getAmount()
                                        .multiply(BigDecimal.valueOf(account.getCurrency().rate)
                                                .round(new MathContext(3, RoundingMode.HALF_UP))), BigDecimal::add));
    }

    /**
     * Zwraca sumę kwadratów wieków wszystkich użytkowników.
     */
    int getAgeSquaresSum() {
        return getUserStream().mapToInt(user -> (int) Math.pow(user.getAge(), 2)).sum();
    }

    /**
     * Metoda zwraca N losowych użytkowników (liczba jest stała). Skorzystaj z metody generate. Użytkownicy nie mogą się powtarzać, wszystkie zmienną
     * muszą być final. Jeżeli podano liczbę większą niż liczba użytkowników należy wyrzucić wyjątek (bez zmiany sygnatury metody).
     */
    List<User> getRandomUsers(final int n) {
        final UserMockGenerator umg = new UserMockGenerator();
        return Optional.of(new UserMockGenerator()
                .generate()
                .stream()
                .limit(n)
                .distinct()
                .collect(collectingAndThen(toList(), users -> {
                            Collections.shuffle(users);
                            return users;
                        })
                )).orElseThrow(() -> new IllegalArgumentException("parameter n is too big"));
    }

    /**
     * Zwraca strumień wszystkich firm.
     */
    private Stream<Company> getCompanyStream() {
        return holdings.stream().flatMap(x -> x.getCompanies().stream());
    }

    /**
     * Zwraca zbiór walut w jakich są rachunki.
     */
    private Set<Currency> getCurenciesSet() {
        return Arrays.stream(Currency.values()).collect(toSet());
    }

    /**
     * Tworzy strumień rachunków.
     */
    private Stream<Account> getAccoutStream() {
        return getUserStream()
                .flatMap(user -> user
                        .getAccounts()
                        .stream());
    }

    /**
     * Tworzy strumień użytkowników.
     */
    private Stream<User> getUserStream() {
        return holdings.stream()
                .flatMap(x -> x.getCompanies().stream()
                        .flatMap(company -> company.getUsers()
                                .stream()
                        ));
    }

}
